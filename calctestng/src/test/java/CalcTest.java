import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CalcTest {

  @DataProvider
  public Object[][] testPositiveSum() {
    return new Object[][]{
        {4, 2, 2},
        {6, 3, 3},
        {2, 1, 1},
        {5, 3, 2}
    };
  }

  @DataProvider
  public Object[][] testNegativeSum() {
    return new Object[][]{
        {5, 2, 2},
        {8, 3, 3},
        {3, 1, 1},
        {2, 3, 2}
    };
  }

  @Test(dataProvider = "testPositiveSum")
  public void sumTest(int sum, int num1, int num2) {
    Assert.assertEquals(sum, Calculator.sum(num1, num2), "Значения не равны");
  }

  @Test(dataProvider = "testNegativeSum")
  public void sumTestNegative(int sum, int num1, int num2) {
    Assert.assertNotEquals(sum, Calculator.sum(num1, num2), "Значения равны");
  }

  @DataProvider
  public Object[][] testPositiveSub() {
    return new Object[][]{
        {0, 2, 2},
        {0, 5, 5},
        {5, 6, 1},
        {6, 5, -1}
    };
  }

  @DataProvider
  public Object[][] testNegativeSub() {
    return new Object[][]{
        {5, 2, 2},
        {8, 3, 3},
        {3, 1, 1},
        {2, 3, 2}
    };
  }

  @Test(dataProvider = "testPositiveSub")
  public void subTest(int sub, int num1, int num2) {
    Assert.assertEquals(sub, Calculator.subtraction(num1, num2), "Значения не равны");
  }

  @Test(dataProvider = "testNegativeSub")
  public void subTestNegative(int sub, int num1, int num2) {
    Assert.assertNotEquals(sub, Calculator.subtraction(num1, num2), "Значения равны");
  }

  @DataProvider
  public Object[][] testPositiveMul() {
    return new Object[][]{
        {4, 2, 2},
        {15, 5, 3},
        {6, 6, 1},
        {-50, 5, -10}
    };
  }

  @DataProvider
  public Object[][] testNegativeMul() {
    return new Object[][]{
        {5, 2, 2},
        {8, 3, 3},
        {3, 1, 1},
        {2, 3, 2}
    };
  }

  @Test(dataProvider = "testPositiveMul")
  public void mulTest(int mul, int num1, int num2) {
    Assert.assertEquals(mul, Calculator.multiplication(num1, num2), "Значения не равны");
  }

  @Test(dataProvider = "testNegativeMul")
  public void mulTestNegative(int mul, int num1, int num2) {
    Assert.assertNotEquals(mul, Calculator.multiplication(num1, num2), "Значения равны");
  }

  @DataProvider
  public Object[][] testPositiveDiv() {
    return new Object[][]{
        {1, 2, 2},
        {5, 5, 1},
        {2, 6, 3},
        {-5, -10, 2}
    };
  }

  @DataProvider
  public Object[][] testNegativeDiv() {
    return new Object[][]{
        {5, 2, 2},
        {8, 3, 3},
        {3, 1, 1},
        {2, 3, 2}
    };
  }

  @Test(dataProvider = "testPositiveDiv")
  public void divTest(int div, int num1, int num2) {
    Assert.assertEquals(div, Calculator.division(num1, num2), "Значения не равны");
  }

  @Test(dataProvider = "testNegativeDiv")
  public void divTestNegative(int div, int num1, int num2) {
    Assert.assertNotEquals(div, Calculator.division(num1, num2), "Значения равны");
  }

}
