import java.util.Scanner;

public class Calculator {

  private static final Scanner scanner = new Scanner(System.in);

  public static void main(String[] args) {
    int num1 = getInt();
    int num2 = getInt();
    char operation = getOperation();
    int result = calculation(num1, num2, operation);
    System.out.println("Результат равен: " + result);
  }

  public static int getInt() {
    System.out.println("Введите любое число: ");
    int num;
    if (scanner.hasNext()) {
      num = scanner.nextInt();
    } else {
      System.out.println("Введено некорректное число. Попробуйте снова: ");
      scanner.next();
      num = getInt();
    }
    return num;
  }

  public static char getOperation() {
    System.out.println("Введите операцию '+', '-', '*', '/' : ");
    char operation;
    if (scanner.hasNext()) {
      operation = scanner.next().charAt(0);
    } else {
      System.out.println("Не выбрана операция. Попробуйте снова.");
      scanner.next();
      operation = getOperation();
    }
    return operation;
  }

  public static int calculation(int num1, int num2, char operation) {
    int result;
    switch (operation) {
      case '+':
        result = sum(num1, num2);
        break;
      case '-':
        result = subtraction(num1, num2);
        break;
      case '*':
        result = multiplication(num1, num2);
        break;
      case '/':
        result = division(num1, num2);
        break;
      default:
        System.out.println("Некорректная операция. Попробуйте снова.");
        result = calculation(num1, num2, getOperation());
    }
    return result;
  }

  public static int sum(int num1, int num2) {
    return num1 + num2;
  }

  public static int subtraction(int num1, int num2) {
    return num1 - num2;
  }

  public static int multiplication(int num1, int num2) {
    return num1 * num2;
  }

  public static int division(int num1, int num2) {
    if (num2 == 0) {
      throw new ArithmeticException();
    }
    return num1 / num2;
  }


}
