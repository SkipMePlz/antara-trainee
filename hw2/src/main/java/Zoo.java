import animals.Cow;
import animals.Duck;
import animals.Fish;
import animals.Hawk;
import animals.Swim;
import animals.Wolf;
import animals.Zebra;
import food.Grass;
import food.Meat;

public class Zoo {

  public static void main(String[] args) {
    Cow cow = new Cow();
    Duck duck = new Duck();
    Fish fish = new Fish();
    Hawk hawk = new Hawk();
    Wolf wolf = new Wolf();
    Zebra zebra = new Zebra();

    Worker worker = new Worker();

    Meat meat = new Meat();
    Grass grass = new Grass();

    worker.feed(wolf, grass);
    worker.feed(wolf, meat);
    worker.getVoice(wolf);
    worker.getVoice(cow);

    Swim[] pond = new Swim[]{duck, fish, wolf, zebra};
    for (Swim animal : pond) {
      animal.swim();
    }

    wolf.setFedRate(100);
    worker.feed(wolf, meat);

  }

}
