package animals;

import food.Food;
import food.Grass;

public abstract class Carnivorous extends Animal {

  @Override
  public void eat(Food food) {
    if (food instanceof Grass) {
      System.out.println("Я не ем траву, я же хищник");
      return;
    }
    if (super.getFedRate() >= 100) {
      System.out.println("Животное уже сыто");
    } else {
      setFedRate(super.getFedRate() + food.getFeedPoints());
      System.out.println("Животное наелось!");
    }
  }
}
