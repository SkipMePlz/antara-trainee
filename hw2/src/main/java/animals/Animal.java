package animals;

import food.Food;

public abstract class Animal {

  private int fedRate = 0;

  public int getFedRate() {
    return fedRate;
  }

  public void setFedRate(int fedRate) {
    this.fedRate = fedRate;
  }

  public abstract void eat(Food food);

}
