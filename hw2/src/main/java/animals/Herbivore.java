package animals;

import food.Food;
import food.Meat;

public abstract class Herbivore extends Animal {

  @Override
  public void eat(Food food) {
    if (food instanceof Meat) {
      System.out.println("Я не ем мясо, я травоядное животное");
      return;
    }
    if (super.getFedRate() >= 100) {
      System.out.println("Животное уже сыто");
    } else {
      setFedRate(super.getFedRate() + food.getFeedPoints());
      System.out.println("Животное наелось!");
    }
  }
}
