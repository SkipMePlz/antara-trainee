package animals;

public class Zebra extends Herbivore implements Run, Swim, Voice {

  @Override
  public void run() {
    System.out.println("Зебра бегает");
  }

  @Override
  public void swim() {
    System.out.println("Зебра плавает (как может)");
  }

  @Override
  public String voice() {
    return "Зебра ржёт *оченьстрашныезвуки*";
  }
}
