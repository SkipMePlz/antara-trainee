package animals;

public class Hawk extends Carnivorous implements Fly, Voice {

  @Override
  public void fly() {
    System.out.println("Ястреб летает");
  }

  @Override
  public String voice() {
    return "Ястреб кричит что-то на ястребином";
  }
}
