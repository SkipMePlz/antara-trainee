package org.example.api;

import static io.restassured.RestAssured.given;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import java.io.IOException;
import java.util.Map;
import java.util.Random;
import org.example.model.Order;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class HomeTaskApiTest {

  private Order orderToDelete;

  @BeforeClass
  public void prepare() throws IOException {
    System.getProperties().load(ClassLoader.getSystemResourceAsStream("test.properties"));
    RestAssured.requestSpecification = new RequestSpecBuilder()
        .setBaseUri(System.getProperty("base.uri"))
        .addHeader("api_key", System.getProperty("api.key"))
        .setAccept(ContentType.JSON)
        .setContentType(ContentType.JSON)
        .log(LogDetail.ALL)
        .build();
    RestAssured.filters(new ResponseLoggingFilter());
    orderToDelete = new Order();
    int id = new Random().nextInt(500000);
    orderToDelete.setId(id);
    orderToDelete.setPetId(12345);
  }

  @Test
  public void makeAnOrder() {
    Order order = new Order();
    int id = new Random().nextInt(500000);
    order.setId(id);
    order.setPetId(12345);
    given()
        .body(order)
        .when()
        .post("/store/order")
        .then()
        .statusCode(200);
    Order actual =
        given().pathParam("orderId", id)
            .get("/store/order/{orderId}")
            .then().statusCode(200).extract().body().as(Order.class);
  }

  @Test
  public void deleteAnOrder() {
    given()
        .body(orderToDelete)
        .when()
        .post("/store/order")
        .then()
        .statusCode(200);

    given().pathParam("orderId", orderToDelete.getId())
        .when().delete("/store/order/{orderId}")
        .then().statusCode(200);
    given().pathParam("orderId", orderToDelete.getId())
        .when().get("/store/order/{orderId}")
        .then().statusCode(404);
  }

  @Test
  public void inventoryTest() {
    Map<String, Integer> inventory =
        given().get("/store/inventory").then().statusCode(200).extract().body().as(Map.class);
    Assert.assertTrue(inventory.containsKey("sold"), "Inventory не содержит статус sold");

  }

}
