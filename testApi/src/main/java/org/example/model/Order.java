package org.example.model;

import com.google.gson.annotations.SerializedName;

public class Order {

  @SerializedName("petId")
  private int petId;
  @SerializedName("quantity")
  private int quantity;
  @SerializedName("id")
  private int id;
  @SerializedName("shipDate")
  private String shipDate;
  @SerializedName("complete")
  private boolean complete;
  @SerializedName("status")
  private String status;

  public int getPetId() {
    return petId;
  }

  public void setPetId(int petId) {
    this.petId = petId;
  }

  public int getQuantity() {
    return quantity;
  }

  public void setQuantity(int quantity) {
    this.quantity = quantity;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getShipDate() {
    return shipDate;
  }

  public void setShipDate(String shipDate) {
    this.shipDate = shipDate;
  }

  public boolean isComplete() {
    return complete;
  }

  public void setComplete(boolean complete) {
    this.complete = complete;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }
}