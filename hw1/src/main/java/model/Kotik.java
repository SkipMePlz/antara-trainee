package model;

public class Kotik {
  private int prettiness;
  private int weight;
  private String name;
  private String meow;
  private static int counter;
  private int fedRate = 0;

  public Kotik() {
    ++counter;
  }

  public Kotik(int prettiness, int weight, String name, String meow) {
    this.prettiness = prettiness;
    this.weight = weight;
    this.name = name;
    this.meow = meow;
    ++counter;
  }

  public void setKotik(int prettiness, int weight, String name, String meow) {
    this.prettiness = prettiness;
    this.weight = weight;
    this.name = name;
    this.meow = meow;
  }

  public void sayMeow(String meow) {
    System.out.println(this.name + " говорит " + meow);
  }

  public boolean play() {
    if(isHungry()) {
      return false;
    }
      fedRate-=3;
    return true;
  }

  public boolean sleep() {
    if(isHungry()) {
      return false;
    }
    fedRate-=1;
    return true;
  }

  public boolean chaseMouse() {
    if(isHungry()) {
      return false;
    }
    fedRate-=4;
    return true;
  }

  public boolean run() {
    if(isHungry()) {
      return false;
    }
    fedRate-=4;
    return true;
  }

  public boolean cleaning() {
    if(isHungry()) {
      return false;
    }
    fedRate-=2;
    return true;
  }

  public void liveAnotherDay() {
    for(int i = 0; i < 24; ++i) {
        switch((int)(Math.random() * 5.0D + 1.0D)) {
          case 1:
            if (this.play()) {
              System.out.println("Котик поиграл");
            } else {
              this.eat(15);
            }
            break;
          case 2:
            if (this.sleep()) {
              System.out.println("Котик поспал");
            } else {
              this.eat(15);
            }
            break;
          case 3:
            if (this.chaseMouse()) {
              System.out.println("Котик погонялся за мышкой");
            } else {
              this.eat(15);
            }
            break;
          case 4:
            if (this.run()) {
              System.out.println("Котик побегал");
            } else {
              this.eat(15);
            }
            break;
          case 5:
            if (this.cleaning()) {
              System.out.println("Котик умылся");
            } else {
              this.eat(15);
            }
        }
    }

  }

  public void eat(int feed) {
    this.fedRate += feed;
    System.out.println("Котик покушал");
  }

  public void eat(int feed, String foodName) {
    this.fedRate += feed;
    System.out.println("Котик покушал");
  }

  public void eat() {
    this.eat(20, "Whiskas");
  }

  public int getPrettiness() {
    return this.prettiness;
  }

  public int getWeight() {
    return this.weight;
  }

  public String getName() {
    return this.name;
  }

  public String getMeow() {
    return this.meow;
  }

  public static int getCounter() {
    return counter;
  }

  public int getFedRate() {
    return this.fedRate;
  }

  public boolean isHungry() {
    return this.fedRate <= 0;
  }
}
