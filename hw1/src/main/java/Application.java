import model.Kotik;

public class Application {
  public Application() {
  }

  public static void main(String[] args) {
    Kotik barsik = new Kotik(80, 5, "Barsik", "Cats rule");
    Kotik timosha = new Kotik();
    timosha.setKotik(60, 7, "Timosha", "Cats rule");
    barsik.liveAnotherDay();
    System.out.println(barsik.getWeight() + "кг весит " + barsik.getName());
    System.out.println(barsik.getMeow().equals(timosha.getMeow()));
    System.out.println("Количество созданных котиков = " + Kotik.getCounter());
  }
}
