import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
    features = "src/main/java/features",
    glue = "stepDef"
)
public class RunnerTest extends AbstractTestNGCucumberTests {

}
