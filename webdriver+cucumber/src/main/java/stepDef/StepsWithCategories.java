package stepDef;

import io.cucumber.java.ParameterType;
import io.cucumber.java.ru.И;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

public class StepsWithCategories {

  private WebDriver driver = WebDriverManager.getDriverManager().getDriver();

  @ParameterType(".*")
  public Categories categories(String category) {
    return Categories.valueOf(category);
  }

  @И("в выпадающем списке категорий выбрана {categories}")
  public void selectedOption(Categories category) {
    Select categories = new Select(
        driver.findElement(By.xpath("//select[@name = \"category_id\"]")));
    categories.selectByVisibleText(category.value);
  }


}
