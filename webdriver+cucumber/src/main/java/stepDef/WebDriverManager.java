package stepDef;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebDriverManager {

  private static WebDriverManager driverManager = null;
  private WebDriver driver;

  private WebDriverManager() {
    System.setProperty("webdriver.chrome.driver", "E:\\driver\\chromedriver.exe");
    driver = new ChromeDriver();
  }

  public static WebDriverManager getDriverManager() {
    if (driverManager == null) {
      driverManager = new WebDriverManager();
    }
    return driverManager;
  }

  public WebDriver getDriver() {
    return driver;
  }

  public void shutdown() {
    driver.quit();
    driverManager = null;
  }
}
