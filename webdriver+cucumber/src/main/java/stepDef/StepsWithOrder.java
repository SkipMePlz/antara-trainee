package stepDef;

import io.cucumber.java.ParameterType;
import io.cucumber.java.ru.И;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class StepsWithOrder {

  private WebDriver driver = WebDriverManager.getDriverManager().getDriver();
  private WebDriverWait wait = new WebDriverWait(driver, 10);

  @ParameterType(".*")
  public Order order(String order) {
    return Order.valueOf(order);
  }

  @И("в выпадающем списке сортировка выбрано значение {order}")
  public void orderSelect(Order order) {
    Select ordering = new Select(driver.findElement(By.xpath(
        "//div[@class=\"sort-select-3QxXG select-select-box-3LBfK select-size-s-2gvAy\"]//select[@class=\"select-select-3CHiM\"]")));
    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
        "//div[@class=\"sort-select-3QxXG select-select-box-3LBfK select-size-s-2gvAy\"]//select[@class=\"select-select-3CHiM\"]")));
    ordering.selectByVisibleText(order.value);
  }

}
