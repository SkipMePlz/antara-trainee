import animals.Animal;
import animals.AviarySizing;
import animals.Carnivorous;
import animals.Herbivore;
import java.util.HashMap;
import java.util.Map;

public class Aviary<E extends Animal> {

  private Map<String, E> cage = new HashMap<>();
  private AviarySizing cageAviarySize = AviarySizing.MEDIUM;


  public void addToAviary(E animal) {
    if (animal.getRequiredAviarySize().compareTo(cageAviarySize) <= 0) {
      cage.put(animal.getName(), animal);
      System.out.println(animal.getName() + ": помещен(а) в вольер");
    } else {
      System.out.println("Вольер не подходит для животного");
    }
  }

  public void deleteFromAviary(E animal) {
    if (cage.containsValue(animal)) {
      cage.remove(animal.getName());
      System.out.println(animal.getName() + " выпущен(а) с вольера");
    } else {
      System.out.println(animal.getName() + " не находится в вольере");
    }
  }

  public E getFromAviary(String name) {
    if (cage.containsKey(name)) {
      return cage.get(name);
    }
    System.out.println(name + " не находится в вольере");
    return null;
  }
}
