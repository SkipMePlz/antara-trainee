import animals.Animal;
import animals.AviarySizing;
import animals.Carnivorous;
import animals.Cow;
import animals.Duck;
import animals.Fish;
import animals.Hawk;
import animals.Herbivore;
import animals.Swim;
import animals.Wolf;
import animals.Zebra;
import food.Grass;
import food.Meat;

public class Zoo {

  public static void main(String[] args) {
    Cow cow = new Cow("Корова");
    Duck duck = new Duck("Утка");
    Fish fish = new Fish("Рыба");
    Hawk hawk = new Hawk("Ястреб");
    Wolf wolf = new Wolf("Волк");
    Zebra zebra = new Zebra("Зебра");

    Worker worker = new Worker();

    Meat meat = new Meat();
    Grass grass = new Grass();

    worker.feed(wolf, grass);

    worker.feed(wolf, meat);
    worker.getVoice(wolf);
    worker.getVoice(cow);

    Swim[] pond = new Swim[]{duck, fish, wolf, zebra};
    for (Swim animal : pond) {
      animal.swim();
    }
    Aviary<Herbivore> herbivoreAviary = new Aviary<>();
    Aviary<Carnivorous> carnivorousAviary = new Aviary<>();
    herbivoreAviary.addToAviary(duck);
    herbivoreAviary.deleteFromAviary(duck);
  }

}
