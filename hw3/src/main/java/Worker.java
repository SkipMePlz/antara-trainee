import animals.Animal;
import animals.Voice;
import food.Food;
import food.WrongFoodException;

public class Worker {

  public void feed(Animal animal, Food food) {
    try {
      animal.eat(food);
    } catch (WrongFoodException e) {
      System.out.println(e + ": Данная пища не подходит животному");
    }

  }

  public void getVoice(Voice animal) {
    System.out.println(animal.voice());
  }

}
