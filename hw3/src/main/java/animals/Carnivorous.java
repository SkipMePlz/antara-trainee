package animals;

import food.Food;
import food.Grass;
import food.WrongFoodException;

public abstract class Carnivorous extends Animal {

  public Carnivorous(String name) {
    super(name);
  }

  @Override
  public void eat(Food food) throws WrongFoodException {
    if (food instanceof Grass) {
      throw new WrongFoodException();
    }
    if (getFedRate() >= 100) {
      System.out.println("Животное уже сыто");
    } else {
      setFedRate(getFedRate() + food.getFeedPoints());
      System.out.println("Животное наелось!");
    }
  }
}
