package animals;

public class Cow extends Herbivore implements Run, Voice {

  public Cow(String name) {
    super(name);
    requiredAviarySize = AviarySizing.LARGE;
  }

  @Override
  public void run() {
    System.out.println("Корова бегает");
  }

  @Override
  public String voice() {
    return "Корова говорит *Мууу*";
  }

}
