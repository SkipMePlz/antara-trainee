package animals;

public class Wolf extends Carnivorous implements Voice, Run, Swim {

  public Wolf(String name) {
    super(name);
    requiredAviarySize = AviarySizing.MEDIUM;
  }

  @Override
  public void run() {
    System.out.println("Волк бегает");
  }

  @Override
  public void swim() {
    System.out.println("Волк плавает");
  }

  @Override
  public String voice() {
    return "Волк воет *аууууу*";
  }
}
