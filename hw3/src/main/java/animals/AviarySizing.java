package animals;

public enum AviarySizing {
  SMALL,
  MEDIUM,
  LARGE,
  EXTRA_LARGE
}
