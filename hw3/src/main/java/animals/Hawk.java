package animals;

public class Hawk extends Carnivorous implements Fly, Voice {

  public Hawk(String name) {
    super(name);
    requiredAviarySize = AviarySizing.SMALL;
  }

  @Override
  public void fly() {
    System.out.println("Ястреб летает");
  }

  @Override
  public String voice() {
    return "Ястреб кричит что-то на ястребином";
  }
}
