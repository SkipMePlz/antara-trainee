package animals;

public class Fish extends Carnivorous implements Swim {

  public Fish(String name) {
    super(name);
    requiredAviarySize = AviarySizing.SMALL;
  }

  @Override
  public void swim() {
    System.out.println("Рыбка плавает");
  }
}
