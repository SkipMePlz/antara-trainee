package animals;

import food.Food;
import food.Meat;
import food.WrongFoodException;

public abstract class Herbivore extends Animal {

  public Herbivore(String name) {
    super(name);
  }

  @Override
  public void eat(Food food) throws WrongFoodException {
    if (food instanceof Meat) {
        throw new WrongFoodException();
    }
    if (getFedRate() >= 100) {
      System.out.println("Животное уже сыто");
    } else {
      setFedRate(getFedRate() + food.getFeedPoints());
      System.out.println("Животное наелось!");
    }
  }
}
