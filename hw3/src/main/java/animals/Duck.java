package animals;

public class Duck extends Herbivore implements Fly, Run, Voice, Swim {

  public Duck(String name) {
    super(name);
    requiredAviarySize = AviarySizing.SMALL;
  }

  @Override
  public void fly() {
    System.out.println("Утка летает");
  }

  @Override
  public void run() {
    System.out.println("Утка бегает");
  }

  @Override
  public void swim() {
    System.out.println("Утка плавает");
  }

  @Override
  public String voice() {
    return "Утка говорит *Кря-кря-кря*";
  }
}
