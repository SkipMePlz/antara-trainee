package animals;

import food.Food;
import food.WrongFoodException;
import java.util.Objects;

public abstract class Animal {

  private String name;
  protected AviarySizing requiredAviarySize;
  private int fedRate = 0;

  public Animal(String name) {
    this.name = name;
  }

  public int getFedRate() {
    return fedRate;
  }

  public void setFedRate(int fedRate) {
    this.fedRate = fedRate;
  }

  public abstract void eat(Food food) throws WrongFoodException;


  public AviarySizing getRequiredAviarySize() {
    return requiredAviarySize;
  }

  public void setRequiredAviarySize(AviarySizing requiredAviarySize) {
    this.requiredAviarySize = requiredAviarySize;
  }

  public String getName() {
    return name;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Animal animal = (Animal) o;
    return Objects.equals(name, animal.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name);
  }
}
