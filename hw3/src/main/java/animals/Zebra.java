package animals;

public class Zebra extends Herbivore implements Run, Swim, Voice {

  public Zebra(String name) {
    super(name);
    requiredAviarySize = AviarySizing.LARGE;
  }

  @Override
  public void run() {
    System.out.println("Зебра бегает");
  }

  @Override
  public void swim() {
    System.out.println("Зебра плавает (как может)");
  }

  @Override
  public String voice() {
    return "Зебра ржёт *оченьстрашныезвуки*";
  }
}
