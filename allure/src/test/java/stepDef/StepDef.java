package stepDef;

import io.cucumber.java.After;
import io.cucumber.java.ru.И;
import io.cucumber.java.ru.Пусть;
import io.cucumber.java.ru.Тогда;
import io.qameta.allure.Step;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;


public class StepDef {

  private WebDriver driver = WebDriverManager.getDriverManager().getDriver();
  private WebDriverWait wait = new WebDriverWait(driver, 10);
  private String region;

  @Step("Открыть ресурс авито")
  @Пусть("открыт ресурс авито")
  public void avitoIsOpened() {
    driver.get("https://www.avito.ru/");
    WebDriverManager.getDriverManager().takeScreenshot(driver);
  }

  @Step("Заполнить поле поиска")
  @И("в поле поиска введено значение {word}")
  public void fillTheSearch(String string) {
    WebElement search = driver
        .findElement(By.xpath("//input[@data-marker=\"search-form/suggest\"]"));
    search.sendKeys(string);
    WebDriverManager.getDriverManager().takeScreenshot(driver);
  }

  @Step("Кликнуть по списку региона")
  @Тогда("кликнуть по выпадающему списку региона")
  public void clickRegionSection() {
    WebElement city = driver
        .findElement(By.xpath("//div[@class=\"main-select-2pf7p main-location-3j9by\"]"));
    city.click();
    WebDriverManager.getDriverManager().takeScreenshot(driver);
  }

  @Step("Корректно введено значение в поле региона")
  @Тогда("в поле регион введено значение {word}")
  public void fillRegionSection(String region) {
    driver.findElement(By.xpath("//input[@class=\"suggest-input-3p8yi\"]")).sendKeys(region);
    this.region = region;
    WebDriverManager.getDriverManager().takeScreenshot(driver);
  }

  @Step("Нажата кнопка показать объявления")
  @И("нажата кнопка показать объявления")
  public void pressShowButton() {
    wait.until(ExpectedConditions
        .visibilityOfElementLocated(
            By.xpath("//span//strong[contains(text(),\"" + region + "\")]")))
        .click();
    WebDriverManager.getDriverManager().takeScreenshot(driver);
    driver.findElement(By.xpath("//button[@data-marker=\"popup-location/save-button\"]")).click();
  }

  @Step("Отображается страница согласно условиям")
  @Тогда("открылась страница результаты по запросу {word}")
  public void pageIsShown(String query) {
    String searchResult = driver
        .findElement(By.xpath("//input[@data-marker=\"search-form/suggest\"]"))
        .getAttribute("value");
    Assert.assertEquals(searchResult, query);
    WebDriverManager.getDriverManager().takeScreenshot(driver);
  }

  @Step("Активирован чекбокс \"только с фотографией\"")
  @И("активирован чекбокс только с фотографией")
  public void photoCheckboxIsActive() {
    WebElement checkbox = driver.findElement(
        By.xpath("//label[@class=\"checkbox-checkbox-7igZ6 checkbox-size-s-yHrZq\"]/span"));
    wait.until(ExpectedConditions.visibilityOf(checkbox));
    if (!checkbox.isSelected()) {
      checkbox.click();
    }
    driver.findElement(By.xpath("//button[@data-marker=\"search-form/submit-button\"]")).click();
    WebDriverManager.getDriverManager().takeScreenshot(driver);
  }

  @Step("Вывод в консоль названия и цены определённого числа товаров")
  @И("в консоль выведено значение названия и цены {int} первых товаров")
  public void shownElements(int number) {
    List<WebElement> printersList = driver.findElements(By.xpath("//div[@data-marker=\"item\"]"));
    if (printersList.size() == 0) {
      System.out.println("По данному запросу нет результатов");
    } else if (printersList.size() < number) {
      for (WebElement webElement : printersList) {
        System.out.println("Название: " +
            webElement.findElement(By.xpath(".//div[@class=\"iva-item-titleStep-2bjuh\"]"))
                .getText()
            + ". Стоимость: " + webElement
            .findElement(By.xpath(".//div[@class=\"iva-item-priceStep-2qRpg\"]")).getText());
      }
    } else {
      for (int i = 0; i < number; i++) {
        System.out.println("Название: " +
            printersList.get(i).findElement(By.xpath(".//div[@class=\"iva-item-titleStep-2bjuh\"]"))
                .getText()
            + ". Стоимость: " + printersList.get(i)
            .findElement(By.xpath(".//div[@class=\"iva-item-priceStep-2qRpg\"]")).getText());
      }
    }
    WebDriverManager.getDriverManager().takeScreenshot(driver);
  }

  @After
  public void shutdown() {
    WebDriverManager.getDriverManager().shutdown();
    driver = null;
    wait = null;
  }
}
